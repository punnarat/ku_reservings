import { Component, OnInit, ViewChild, ElementRef, HostListener } from '@angular/core';
import { DatePipe } from '@angular/common';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { SeatService } from '../seat.service';
import { Seat } from '../seat.model';
import { fromEvent } from 'rxjs';

@Component({
  selector: 'app-reservingadminmanage',
  templateUrl: './reservingadminmanage.component.html',
  styleUrls: ['./reservingadminmanage.component.css']
})
export class ReservingadminmanageComponent implements OnInit {

  // seats : Seat[] 
  mode = 0;
  histories: any;
  lastSelectedSeat: any;
  selectedHistory: any;
  seatdata: any
  typedata: any
  perioddata: any;
  isFormReady = false;
  reload = true;
  seletedDate:any;
  changeFinanceDate(event: any) {
    console.log(event);
    this.seletedDate = event.target.value;
    this.getFinancialData(event.target.value)
  }
  constructor(private router: Router, private http: HttpClient, private seatservice: SeatService, private formBuilder: FormBuilder, public datePipe: DatePipe) {
    this.seatInfoForm = formBuilder.group({})
    this.isFormReady = true
  }
  @ViewChild('canvas') canvas: ElementRef;
  cx: CanvasRenderingContext2D;
  canvasEl: HTMLCanvasElement;
  ratio: number;
  seats: any;

  @HostListener('window:resize', ['$event'])
  onResize(event?) {
    this.canvasEl.height = window.innerHeight;
    this.canvasEl.width = window.innerWidth;
    this.ratio = window.innerHeight / 1000;

    // this.ngAfterViewInit()
    console.log(this.ratio);
    // this.can
    // console.log(window.innerWidth);
    // this.innerWidth = window.innerWidth;
  }

  ngOnInit() {
    this.canvasEl = this.canvas.nativeElement;
    this.cx = this.canvasEl.getContext('2d');

    fromEvent(this.canvasEl, 'mousedown').subscribe(location => {
      //console.log(location);
      //console.log(this.ratio);
      this.seats.forEach(element => {
        //console.log(element);

        if ((location as MouseEvent).layerX / this.ratio >= element.displayX - 5 && (location as MouseEvent).layerX / this.ratio <= element.displayX + 5 &&
          (location as MouseEvent).layerY / this.ratio >= element.displayY - 5 && (location as MouseEvent).layerY / this.ratio <= element.displayY + 5) {
          console.log(element);
          if (element.scheduleStatusId == '00000000-0000-0000-0000-000000000000') {
            this.seatInfoForm.patchValue({
              id: element.id,
              seatId: element.seatId,
              seatCode: element.seatCode,
              seatName: element.seatName,
              // scheduleDate: element.scheduleDate,
              // periodId: element.periodId,
              // periodCode: element.periodCode,
              // periodName: element.periodName,
              // availablePeriod: element.availablePeriod,
              scheduleStatusId: element.scheduleStatusId,
              scheduleStatusCode: element.scheduleStatusCode,
              scheduleStatusName: element.scheduleStatusName,
              displayX: element.displayX,
              displayY: element.displayY,
              // userName: element.userName
            })
            if (this.lastSelectedSeat) {
              this.draw(this.lastSelectedSeat.displayX * this.ratio, this.lastSelectedSeat.displayY * this.ratio, this.lastSelectedSeat);
            }
            this.selectedDraw(element.displayX * this.ratio, element.displayY * this.ratio);
            this.lastSelectedSeat = element;
          }
        }
      });
    }
    );
    this.onResize();
    this.periods();
   // this.seatlist()
    // this.create()
    this.seattypes()

  }
  selectedDraw(x, y) {


    // this.cx.fillStyle = '#000000';
    // this.cx.
    console.log(x);
    this.cx.beginPath();
    this.cx.arc(x, y, 7 * this.ratio, 0, 2 * Math.PI);

    this.cx.fillStyle = 'yellow'


    // if(draw.seatStatusId == '10000000-0000-0000-0000-000000000000'){
    // this.cx.fillStyle = 'gray';
    // } else {}
    // this.cx.fillStyle = 'green';
    // this.cx.fillStyle = 'green';
    this.cx.fill();
  }

  draw(x, y, draw) {
    this.cx.beginPath();
    this.cx.arc(x, y, 7 * this.ratio, 0, 2 * Math.PI);
    switch (draw.scheduleStatusId) {
      case '10000000-0000-0000-0000-000000000000':
        this.cx.fillStyle = '#FFFFFF'
        break;
      case '00000000-0000-0000-0000-000000000000':
        this.cx.fillStyle = '#4fcc25'
        break;
      // case '20000000-0000-0000-0000-000000000000':
      //   this.cx.fillStyle = 'red'
      //   break;
      // case '30000000-0000-0000-0000-000000000000':
      //   this.cx.fillStyle = 'gray'
      //   break;
    }
    this.cx.fill();
  }
  removeDraw(x, y, draw) {
    this.cx.beginPath();
    this.cx.arc(x, y, 7 * this.ratio, 0, 2 * Math.PI);
    this.cx.fillStyle = '#FFFFFF'
    this.cx.fill();
  }
  historyDraw(x, y) {
    this.cx.beginPath();
    this.cx.arc(x, y, 7 * this.ratio, 0, 2 * Math.PI);
    this.cx.fillStyle = '#FF0000'
    this.cx.fill();
  }

  getSeatSchedule(date, period) {
    this.seatservice.getseatSearch(date, period).subscribe(result => {
     // console.log(result)
      if (this.seats) {
        this.seats.forEach(seat => {
          //console.log(seat)
          this.removeDraw(seat.displayX * this.ratio, seat.displayY * this.ratio, seat);
        });
      }
      this.seats = result.data;
      this.seats.forEach(seat => {
        //console.log(seat);
        //console.log(this.selectedHistory);
        if (seat.seatTypeId == this.selectedHistory.seatTypeId) {
          //console.log(seat)
          this.draw(seat.displayX * this.ratio, seat.displayY * this.ratio, seat);
        }
        if (seat.seatId == this.selectedHistory.seatId) {
          this.historyDraw(seat.displayX * this.ratio, seat.displayY * this.ratio);
        }
      });
    });
  }

  //seatInfo:any;
  seatInfoForm: FormGroup;
  isSeatInfoReady = 0;
  showSeatInfo(history: any) {
    console.log(history);
    this.selectedHistory = history;
    this.seatInfoForm = this.formBuilder.group(history);
    this.isSeatInfoReady = 1;
    console.log(this.seatInfoForm);
    this.mode = 1;
    this.getSeatSchedule(history.scheduleDate, history.periodCode);
    //this.reload=false;
  }

  changeSeat() {
    console.log(this.seatInfoForm.getRawValue());
    this.seatservice.changereserveseat(this.seatInfoForm.getRawValue()).subscribe(result => {
      console.log(result);
      alert('Success');
      this.mode = 0;
      this.getFinancialData(this.seletedDate);
      // location.reload()
    });
  }

  getFinancialData(scheduleDate: string) {
    console.log('Change Date');
    this.seatservice.getseat(scheduleDate).subscribe(result => {
      console.log(result);
      this.mode = 0;
      this.seats = result.data;
    });
  
  }
  admin() {
    return this.router.navigate(['admin'])
  }
  device() {
    return this.router.navigate(['device'])
  }
  adminmanage() {
    return this.router.navigate(['adminmanage'])
  }
  closeschedule() {
    return this.router.navigate(['closeschedule'])
  }
  reservingadminmanage() {
    return this.router.navigate(['reservingadminmanage'])
  }
  logout() {
    return this.router.navigate(['login'])
  }
  loginuser() {
    return this.router.navigate(['loginuser'])
  }
  cancelpage() {
    // this.seatservice.getseat(scheduleDate).subscribe(result => {
    //   console.log(result);
    console.log('Cancel');  
    this.mode = 0;
    
    this.getFinancialData(this.seletedDate);
     // this.reload = true
    //   this.seats = result.data;
    // });
  }

  periods() {
    var data = {
      'apiRequest': { 'action': 'list' }
    }
    this.http.post('http://roomcontrol.ctt-center.com/api/core/Period', data)
      .subscribe((res: any) => {
        this.perioddata = res.data
        console.log(res.data);
      })
  }

  seattypes() {
    var data = {
      'apiRequest': { 'action': 'list' }
    }
    this.http.post('http://roomcontrol.ctt-center.com/api/core/SeatType', data)
      .subscribe((res: any) => {
        this.typedata = res.data
        console.log(res.data);
      })
  }
  timedata = [{
    id: 1, hour: ['1']
  },
  {
    id: 2, hour: ['2']
  },
  {
    id: 3, hour: ['3']
  }
  ];

  cancel(seat) {
    var dialog = confirm(seat.seatName)
    if (dialog == true) {
      this.seatservice.cancleseat(seat).subscribe(result => {
        console.log(result);
        alert('Cancel Data!')
      });
    }
  }

  // seatlist() {
  //   var data = {
  //     'apiRequest': { 'action': 'list' }
  //   }
  //   this.http.post('http://roomcontrol.ctt-center.com/api/core/Seat', data)
  //     .subscribe((res: any) => {
  //       this.seatdata = res.data
  //       console.log(res.data);
  //     })
  // }
}
