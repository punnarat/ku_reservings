import { Component, ViewChild, ElementRef, OnInit, HostListener, AfterViewInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { SeatService } from 'src/app/seat.service';
import { fromEvent } from 'rxjs';
import { DatePipe } from '@angular/common';
import { Seat } from 'src/app/seat.model';

@Component({
  selector: 'app-setadmin',
  templateUrl: './setadmin.component.html',
  styleUrls: ['./setadmin.component.css']
})
export class SetadminComponent implements OnInit {

  reserving: FormGroup
  loginusers: FormGroup
  seat: Seat[] = []
  histories: any;
  selectedHistory: any;
  seatdata: any
  typedata: any
  perioddata: any;
  manageseat: any;
  mode = 0; //0 List,1-Edit
  lastSelectedSeat: any;
  isFormReady = false;

  constructor(private router: Router, private modalService: NgbModal, private formBuilder: FormBuilder,
    private seatservice: SeatService,
    public datePipe: DatePipe,
    private http: HttpClient) {
    // this.seatservice.getseatchange().subscribe(result => {
    //   console.log(result)
    //   this.seat = result.data;
    //   this.seatInfoForm = this.formBuilder.group(result.data[0]);

    //   this.manageseat = this.seat.find(f => f.id == this.seatservice.getData())
    //   console.log(this.manageseat)
    this.seatInfoForm = formBuilder.group({})
    this.isFormReady = true
    // });
  }

  @ViewChild('canvas') canvas: ElementRef;
  cx: CanvasRenderingContext2D;
  canvasEl: HTMLCanvasElement;
  ratio: number;
  seats: any;
  submitted = false

  @HostListener('window:resize', ['$event'])
  onResize(event?) {
    this.canvasEl.height = window.innerHeight;
    this.canvasEl.width = window.innerWidth;
    this.ratio = window.innerHeight / 1000;

    // this.ngAfterViewInit()
    console.log(this.ratio);
    // this.can
    // console.log(window.innerWidth);
    // this.innerWidth = window.innerWidth;
  }

  ngOnInit() {
    this.canvasEl = this.canvas.nativeElement;
    this.cx = this.canvasEl.getContext('2d');

    fromEvent(this.canvasEl, 'mousedown').subscribe(location => {
      //console.log(location);
      //console.log(this.ratio);
      this.seats.forEach(element => {
        //console.log(element);

        if ((location as MouseEvent).layerX / this.ratio >= element.displayX - 5 && (location as MouseEvent).layerX / this.ratio <= element.displayX + 5 &&
          (location as MouseEvent).layerY / this.ratio >= element.displayY - 5 && (location as MouseEvent).layerY / this.ratio <= element.displayY + 5) {
          console.log(element);
          if (element.scheduleStatusId == '00000000-0000-0000-0000-000000000000') {
            this.seatInfoForm.patchValue({
              id: element.id,
              seatId: element.seatId,
              seatCode: element.seatCode,
              seatName: element.seatName,
              // scheduleDate: element.scheduleDate,
              // periodId: element.periodId,
              // periodCode: element.periodCode,
              // periodName: element.periodName,
              // availablePeriod: element.availablePeriod,
              scheduleStatusId: element.scheduleStatusId,
              scheduleStatusCode: element.scheduleStatusCode,
              scheduleStatusName: element.scheduleStatusName,
              displayX: element.displayX,
              displayY: element.displayY,
              // userName: element.userName
            })
            if (this.lastSelectedSeat) {
              this.draw(this.lastSelectedSeat.displayX * this.ratio, this.lastSelectedSeat.displayY * this.ratio, this.lastSelectedSeat);
            }
            this.selectedDraw(element.displayX * this.ratio, element.displayY * this.ratio);
            this.lastSelectedSeat = element;
          }
        }
      });
    }
    );
    this.onResize();
    this.periods();
    this.seatlist()
    this.create()
    // this.seathistory()

  }

  create() {
    this.loginusers = this.formBuilder.group({
      userName: ['', Validators.required],
      password: ['', Validators.required]
    })
  }
  selectedDraw(x, y) {


    // this.cx.fillStyle = '#000000';
    // this.cx.
    console.log(x);
    this.cx.beginPath();
    this.cx.arc(x, y, 7 * this.ratio, 0, 2 * Math.PI);

    this.cx.fillStyle = 'yellow'


    // if(draw.seatStatusId == '10000000-0000-0000-0000-000000000000'){
    // this.cx.fillStyle = 'gray';
    // } else {}
    // this.cx.fillStyle = 'green';
    // this.cx.fillStyle = 'green';
    this.cx.fill();
  }
  draw(x, y, draw) {
    this.cx.beginPath();
    this.cx.arc(x, y, 7 * this.ratio, 0, 2 * Math.PI);
    switch (draw.scheduleStatusId) {
      case '10000000-0000-0000-0000-000000000000':
        this.cx.fillStyle = 'red'
        break;
      case '00000000-0000-0000-0000-000000000000':
        this.cx.fillStyle = '#4fcc25'
        break;
      case '20000000-0000-0000-0000-000000000000':
        this.cx.fillStyle = 'blue'
        break;
      case '30000000-0000-0000-0000-000000000000':
        this.cx.fillStyle = 'gray'
        break;
    }
    this.cx.fill();
  }
  removeDraw(x, y, draw) {
    this.cx.beginPath();
    this.cx.arc(x, y, 7 * this.ratio, 0, 2 * Math.PI);
    this.cx.fillStyle = '#FFFFFF'
    this.cx.fill();
  }
  historyDraw(x, y) {
    this.cx.beginPath();
    this.cx.arc(x, y, 7 * this.ratio, 0, 2 * Math.PI);
    this.cx.fillStyle = '#FF0000'
    this.cx.fill();
  }
  // seard(date, period) {

  //   if (period != '') {
  //     var temPeriod = this.perioddata.find(x => x.id == period)
  //     var codeP: any = temPeriod.code
  //   } else {
  //     var codeP: any = ''
  //   }

  //   console.log(date, codeP)
  //   this.seatservice.getseatSearch(date, codeP).subscribe(result => {
  //     console.log(result)
  //     this.seats = result.data;
  //     this.seats.forEach(seat => {
  //       console.log(seat)
  //       this.draw(seat.displayX * this.ratio, seat.displayY * this.ratio, seat);
  //     });
  //   });
  // }

  getSeatSchedule(date, period) {
    this.seatservice.getseatSearch(date, period).subscribe(result => {
      console.log(result)
      if (this.seats) {
        this.seats.forEach(seat => {
          //console.log(seat)
          this.removeDraw(seat.displayX * this.ratio, seat.displayY * this.ratio, seat);
        });
      }
      this.seats = result.data;
      this.seats.forEach(seat => {
        console.log(seat);
        console.log(this.selectedHistory);
        if (seat.seatTypeId == this.selectedHistory.seatTypeId) {
          //console.log(seat)
          this.draw(seat.displayX * this.ratio, seat.displayY * this.ratio, seat);
        }
        if (seat.seatId == this.selectedHistory.seatId) {
          this.historyDraw(seat.displayX * this.ratio, seat.displayY * this.ratio);
        }
      });
    });
  }

  //seatInfo:any;
  seatInfoForm: FormGroup;
  isSeatInfoReady = 0;
  showSeatInfo(history: any) {
    console.log(history);
    this.selectedHistory = history;
    this.seatInfoForm = this.formBuilder.group(history);
    this.isSeatInfoReady = 1;
    console.log(this.seatInfoForm);
    this.mode = 1;
    this.getSeatSchedule(history.scheduleDate, history.periodCode);
  }

  // update(history: any) {
  // this.mode = 1;
  // console.log(history)
  // this.seatservice.setData(history)
  // this.getSeatSchedule(history.scheduleDate, history.periodCode);
  // }

  // navigate
  admin() {
    return this.router.navigate(['admin'])
  }
  setadmin() {
    return this.router.navigate(['setadmin'])
  }
  loginuser() {
    return this.router.navigate(['loginuser'])
  }
  loginadmin() {
    return this.router.navigate(['loginadmin'])
  }
  login() {
    return this.router.navigate(['login'])
  }
  // save
  seathistory(username: string) {
    var data = {
      'apiRequest': { 'action': 'history' }
      , data: [{
        userName: username
      }]
    }
    this.http.post('http://roomcontrol.ctt-center.com/api/core/SeatSchedule', data)
      .subscribe((res: any) => {
        this.histories = res.data
        //console.log(res.data);
      })
  }


  // cancleseat: FormGroup
  // isCancleSeatReady = 0;

  cancel(seat) {
    var dialog = confirm(seat.seatName)
    if (dialog == true) {
      this.seatservice.cancleseat(seat).subscribe(result => {
        console.log(result);
        alert('Cancel Data!')
        this.seathistory(this.loginusers.value.userName)
      });
    }
  }
  get f() { return this.loginusers.controls; }
  loginhistory() {
    this.submitted = true;
    {
      if (this.loginusers.invalid) {
        return;
      }
      console.log(this.loginusers.value);
      // console.log(this.reserving.get('data').value)
      this.seatservice.login(this.loginusers.value).subscribe(data => {
        //console.log(data);
        if (data.apiResponse.id == 0) {
          this.seathistory(this.loginusers.value.userName)
          this.mode = 2;
        }
        else //if (data.apiResponse.id==-1)
        {
          alert(data.apiResponse.desc)
        }
        // else
        // {
        // }
      });
    }
  }

  loginemty() {
    this.submitted = true;
    {
      if (this.loginusers.invalid) {
        return;
      }
      console.log(this.loginusers.value);
      // console.log(this.reserving.get('data').value)
      this.seatservice.loginemty(this.loginusers.value).subscribe(data => {
        //console.log(data);
        if (data.apiResponse.id == 0) {
        }
        else {
          alert(data.apiResponse.desc)
        }
        // else
        // {
        // }
      });
      alert('success');
    }
  }

  logout() {
    location.reload();
  }

  open(content) {
    this.modalService.open(content, { size: 'lg' });
  }

  changeSeat() {
    console.log(this.seatInfoForm.getRawValue());
    this.seatservice.changereserveseat(this.seatInfoForm.getRawValue()).subscribe(result => {
      console.log(result);
      this.mode = 2;
      this.seathistory(this.loginusers.value.userName)
      location.reload()
      alert('Success');
    });
  }

  timedata = [{
    id: 1, hour: ['1']
  },
  {
    id: 2, hour: ['2']
  },
  {
    id: 3, hour: ['3']
  }
  ];

  periods() {
    var data = {
      'apiRequest': { 'action': 'list' }
    }
    this.http.post('http://roomcontrol.ctt-center.com/api/core/Period', data)
      .subscribe((res: any) => {
        this.perioddata = res.data
        console.log(res.data);
      })
  }

  // seattypes() {
  //   var data = {
  //     'apiRequest': { 'action': 'list' }
  //   }
  //   this.http.post('http://roomcontrol.ctt-center.com/api/core/SeatType', data)
  //     .subscribe((res: any) => {
  //       this.typedata = res.data
  //       console.log(res.data);
  //     })
  // }
  seatlist() {
    var data = {
      'apiRequest': { 'action': 'list' }
    }
    this.http.post('http://roomcontrol.ctt-center.com/api/core/Seat', data)
      .subscribe((res: any) => {
        this.seatdata = res.data
        console.log(res.data);
      })
  }


}