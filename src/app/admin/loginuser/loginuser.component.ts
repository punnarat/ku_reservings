import { Component, ViewChild, ElementRef, OnInit, HostListener, AfterViewInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { SeatService } from 'src/app/seat.service';
import { fromEvent, interval } from 'rxjs';
import { DatePipe } from '@angular/common';
import { SeatstatusComponent } from '../seatstatus/seatstatus.component';

@Component({
  selector: 'app-loginuser',
  templateUrl: './loginuser.component.html',
  styleUrls: ['./loginuser.component.css']
})
export class LoginuserComponent implements OnInit {
  reserving: FormGroup
  perioddata: any
  seatdata: any
  typedata: any
  lastSelectedSeat: any;
  isFormReady = false;
  mode = 0; //0 List,1-Edit

  get today() {
    return new Date(), 'dd-MM-yy';
  }
  @ViewChild("divCanvas") divCanvas: ElementRef;
  @ViewChild('canvas') canvas: ElementRef;

  cx: CanvasRenderingContext2D;
  canvasEl: HTMLCanvasElement;
  ratio: number;
  seats: any;
  submitted = false;
  firstLoad = true;
  //width: number;
  //height: number;

  @HostListener('window:resize', ['$event'])
  onResize(event?) {
    if (this.firstLoad)
    {
        this.canvasEl.height = window.innerHeight;
        this.canvasEl.width =window.innerWidth
        this.ratio =window.innerHeight / 1000;
        this.firstLoad = false;
         }
    else
    {
    if (this.divCanvas.nativeElement.offsetWidth>=this.divCanvas.nativeElement.offsetHeight)
    {
      this.canvasEl.height = this.divCanvas.nativeElement.offsetHeight;
      this.canvasEl.width =this.divCanvas.nativeElement.offsetHeight;
      this.ratio = this.divCanvas.nativeElement.offsetHeight / 1000;
      console.log('height');
    }
    else
    {
      this.canvasEl.height = this.divCanvas.nativeElement.offsetWidth-30;
      this.canvasEl.width =this.divCanvas.nativeElement.offsetWidth-30;
      this.ratio = (this.divCanvas.nativeElement.offsetWidth-30) / 1000;
      console.log('width');
    }
  }
 
   
    
    //console.log(this.divCanvas.nativeElement.offsetWidth);
  if (this.seats)
    {
      this.seats.forEach(seat => {
      this.draw(seat.displayX * this.ratio, seat.displayY * this.ratio, seat);
    });
  }
  

    // this.ngAfterViewInit()
    console.log(this.ratio);
    // this.can
    // console.log(window.innerWidth);
    // this.innerWidth = window.innerWidth;
  }
  // private refreshInterval$ = interval(60000);
  constructor(private router: Router, private modalService: NgbModal, private formBuilder: FormBuilder,
    private seatservice: SeatService,
    public datePipe: DatePipe,
    private http: HttpClient,
  ) {
    // this.seatservice.getallseat().subscribe(result => {
    //   console.log(result);
    //   this.reserving = result.data;

    //   // config.outsideDays = 'hidden';
    //   // config.markDisabled = (date: NgbDate) => calendar.getWeekday(date) >= 7;

    //   // this.isFormReady = true;
    //   // console.log(this.reserving);
    // });

  }

  getSeatSchedule() {
    console.log(this.reserving.value);
    var params = { apiRequest: { action: "get" }, data: [this.reserving.value] };

    this.http.post("http://roomcontrol.ctt-center.com/api/core/seatschedule", params).subscribe((response:any) => {
      this.isFormReady = false;
      this.seats.forEach(seat => {
        this.removeDraw(seat.displayX * this.ratio, seat.displayY * this.ratio);
      });
      this.seats = response.data;
      this.seats.forEach(seat => {
        this.draw(seat.displayX * this.ratio, seat.displayY * this.ratio, seat);
      });
      this.isFormReady = true;
      //console.log(this.seats);
    });
  }

  ngOnInit() {
    this.canvasEl = this.canvas.nativeElement;
    this.cx = this.canvasEl.getContext('2d');
    var params = { apiRequest: { action: "get_default" } };
    var data = {
      'apiRequest': { 'action': 'list' }
    }
    this.http.post('http://roomcontrol.ctt-center.com/api/core/SeatType', data)
      .subscribe((res: any) => {
        this.typedata = res.data;
        this.http.post("http://roomcontrol.ctt-center.com/api/core/seatschedule", params).subscribe((response:any) => {
          this.seats = response.data;
          this.seats.forEach(seat => {
            this.draw(seat.displayX * this.ratio, seat.displayY * this.ratio, seat);
            //Finish Drawing

            const params = { apiRequest: { action: "get" }, data: [{ scheduleDate: new Date('dd/mm/yyyy') }] }

            this.http.post("http://roomcontrol.ctt-center.com/api/core/period", params).subscribe((response:any) => {
              // console.log(response.data);
              this.perioddata = response.data;
              

              var date = this.datePipe.transform(new Date(), 'y-MM-dd')
              const newreserving = {
                id: '',
                seatId: '',
                seatCode: '',
                seatName: '',
                scheduleDate: [date, Validators.required],
                periodId: this.seats[0].periodId,
                seatTypeId: '00000000-0000-0000-0000-000000000000',
                periodCode: '',
                periodName: '',
                slots: '1',
                scheduleStatusId: '',
                scheduleStatusCode: '',
                scheduleStatusName: '',
                displayX: '',
                displayY: '',
                userName: ['', Validators.required],
                password: ['', Validators.required]
                //seatTypeId: '' //'00000000-0000-0000-0000-000000000000'//: this.seats[0].seatTypeId,
              }

              this.reserving = this.formBuilder.group(newreserving);
              // this.seattypes();
              this.isFormReady = true;
            });

            //Finish create form

          });
          //console.log(this.seats);
        });
      });


    fromEvent(this.canvasEl, 'mousedown').subscribe(location => {
      //console.log(location);
      //console.log(this.ratio);

      this.seats.forEach(element => {

        //console.log(element);

        if ((location as MouseEvent).layerX / this.ratio >= element.displayX - 5 && (location as MouseEvent).layerX / this.ratio <= element.displayX + 5 &&
          (location as MouseEvent).layerY / this.ratio >= element.displayY - 5 && (location as MouseEvent).layerY / this.ratio <= element.displayY + 5) {
          console.log(element);
          if (element.scheduleStatusId == '00000000-0000-0000-0000-000000000000') {
            this.reserving.patchValue({
              id: element.id,
              seatId: element.seatId,
              seatCode: element.seatCode,
              seatName: element.seatName,
              // periodId: element.periodId,
              // periodCode: element.periodCode,
              // periodName: element.periodName,
              // availablePeriod: element.availablePeriod,
              scheduleStatusId: element.scheduleStatusId,
              scheduleStatusCode: element.scheduleStatusCode,
              scheduleStatusName: element.scheduleStatusName,
              displayX: element.displayX,
              displayY: element.displayY,
              seatTypeId: element.seatTypeId
            })
            if (this.lastSelectedSeat) {
              this.draw(this.lastSelectedSeat.displayX * this.ratio, this.lastSelectedSeat.displayY * this.ratio, this.lastSelectedSeat);
            }
            this.selectedDraw(element.displayX * this.ratio, element.displayY * this.ratio);
            this.lastSelectedSeat = element;
            //this.selectedDraw(element.displayX,element.displayY);
          }
        }

      });

      // this.seats.forEach(element => {

      //   //console.log(element);

      //   if ((location as MouseEvent).layerX / this.ratio >= element.displayX - 5 && (location as MouseEvent).layerX / this.ratio <= element.displayX + 5 &&
      //     (location as MouseEvent).layerY / this.ratio >= element.displayY - 5 && (location as MouseEvent).layerY / this.ratio <= element.displayY + 5) {
      //     console.log(element);
      //     if (element.scheduleStatusId == '00000000-0000-0000-0000-000000000000') {
      //       this.reserving.patchValue({
      //         id: element.id,
      //         seatId: element.seatId,
      //         seatCode: element.seatCode,
      //         seatName: element.seatName,
      //         // periodId: element.periodId,
      //         // periodCode: element.periodCode,
      //         // periodName: element.periodName,
      //         // availablePeriod: element.availablePeriod,
      //         scheduleStatusId: element.scheduleStatusId,
      //         scheduleStatusCode: element.scheduleStatusCode,
      //         scheduleStatusName: element.scheduleStatusName,
      //         displayX: element.displayX,
      //         displayY: element.displayY
      //       })
      //     }
      //   }
      // });
    }
    );
    this.onResize();
    
    // this.refreshInterval$.subscribe(() => {
    //   window.location.reload();
    //   // this.getSeatSchedule()
    //   console.log('ss')
    //           }
      
    //       );
  }

  // ngAfterViewInit() {

  //   this.seatservice.getseat(date).subscribe(result => {

  //     this.seats = result.data;
  //     this.seats.forEach(seat => {

  //       this.draw(seat.displayX * this.ratio, seat.displayY * this.ratio);
  //     });

  //   });
  // }
  selectedDraw(x, y) {


    // this.cx.fillStyle = '#000000';
    // this.cx.
    console.log(x);
    this.cx.beginPath();
    this.cx.arc(x, y, 7 * this.ratio, 0, 2 * Math.PI);

    this.cx.fillStyle = 'yellow'


    // if(draw.seatStatusId == '10000000-0000-0000-0000-000000000000'){
    // this.cx.fillStyle = 'gray';
    // } else {}
    // this.cx.fillStyle = 'green';
    // this.cx.fillStyle = 'green';
    this.cx.fill();
  }
  draw(x, y, draw) {


    // this.cx.fillStyle = '#000000';
    // this.cx.
    this.cx.beginPath();
    this.cx.arc(x, y, 7 * this.ratio, 0, 2 * Math.PI);
    switch (draw.scheduleStatusId) {
      case '10000000-0000-0000-0000-000000000000':
        this.cx.fillStyle = 'red'
        break;
      case '00000000-0000-0000-0000-000000000000':
        this.cx.fillStyle = '#4fcc25'
        break;
      case '20000000-0000-0000-0000-000000000000':
        this.cx.fillStyle = 'blue'
        break;
      case '30000000-0000-0000-0000-000000000000':
        this.cx.fillStyle = 'gray'
        break;
    }

    // if(draw.seatStatusId == '10000000-0000-0000-0000-000000000000'){
    // this.cx.fillStyle = 'gray';
    // } else {}
    // this.cx.fillStyle = 'green';
    // this.cx.fillStyle = 'green';
    this.cx.fill();
  } 
  removeDraw(x, y) {
    // this.cx.fillStyle = '#000000';
    // this.cx.
    this.cx.beginPath();
    this.cx.arc(x, y, 7 * this.ratio, 0, 2 * Math.PI);
    this.cx.fillStyle = 'white'
     
    this.cx.fill();
  }

  timedata = [{
    id: 1, hour: ['1']
  },
  {
    id: 2, hour: ['2']
  },
  {
    id: 3, hour: ['3']
  }
  ];

  // periods() {
  //   var data = {
  //     'apiRequest': { 'action': 'list' }
  //   }
  //   this.http.post('http://roomcontrol.ctt-center.com/api/core/Period', data)
  //     .subscribe((res: any) => {
  //       this.perioddata = res.data
  //       console.log(res.data);
  //     })
  // }

  // seattypes() {
  //   var data = {
  //     'apiRequest': { 'action': 'list' }
  //   }
  //   this.http.post('http://roomcontrol.ctt-center.com/api/core/SeatType', data)
  //     .subscribe((res: any) => {
  //       this.typedata = res.data
  //       console.log(res.data);
  //     })
  // }
  seat() {
    var data = {
      'apiRequest': { 'action': 'list' }
    }
    this.http.post('http://roomcontrol.ctt-center.com/api/core/Seat', data)
      .subscribe((res: any) => {
        this.seatdata = res.data
        console.log(res.data);
      })
  }

  // popup
  open(content) {
    this.modalService.open(content, { size: 'lg' });
  }


  // navigate
  admin() {
    return this.router.navigate(['admin'])
  }
  setadmin() {
    return this.router.navigate(['setadmin'])
  }
  loginuser() {
    return this.router.navigate(['loginuser'])
  }
  loginadmin() {
    return this.router.navigate(['loginadmin'])
  }
  login() {
    return this.router.navigate(['login'])
  }

  getPeriodForDate(date, slot, period) {
    const params = { apiRequest: { action: "get" }, data: [{ scheduleDate: date, slots: slot, periodId: period }] }
    this.http.post("http://roomcontrol.ctt-center.com/api/core/period", params).subscribe(response => {
      
      // console.log(response.data);
      // this.perioddata = period;
    });

    // const paramtype = { apiRequest: { action: "get" }, data: [{ scheduleDate: date }] }
    // this.http.post("http://roomcontrol.ctt-center.com/api/core/SeatType", paramtype).subscribe(response => {
    //   // console.log(response.data);
    //   this.typedata = response.data;
    // });
    // this.seatservice.get().subscribe(result => {
    //   console.log(result)
    //   this.seats = result.data;
    //   this.seats.forEach(seat => {
    //     console.log(seat)
    //     this.draw(seat.displayX * this.ratio, seat.displayY * this.ratio, seat);
    //   });
    // });
  }
  // getDefaultSeatSchedule() {
  //   const params = { apiRequest: { action: "get_default" },   }
  //   this.http.post("http://roomcontrol.ctt-center.com/api/core/seatschedule", params).subscribe(response => {
  //     // console.log(response.data);
  //     this.perioddata = response.data;
  //   });
  // }
  // seard
  // seard(date, period) {

  //   if (period != '') {
  //     var temPeriod = this.perioddata.find(x => x.id == period)
  //     var codeP: any = temPeriod.code
  //   } else {
  //     var codeP: any = ''
  //   }

  //   console.log(date, codeP)
  //   this.seatservice.getseatSearch(date, codeP).subscribe(result => {
  //     console.log(result)
  //     this.seats = result.data;
  //     this.seats.forEach(seat => {
  //       console.log(seat)
  //       this.draw(seat.displayX * this.ratio, seat.displayY * this.ratio, seat);
  //     });
  //   });
  // }

  // save
  refresh(){
    window.location.reload()
  }
  get f() { return this.reserving.controls; }
  reserve() {
    this.submitted = true;
    {
      if (this.reserving.invalid) {
        return;
      }
      console.log(this.reserving.value);
      // console.log(this.reserving.get('data').value)
      this.seatservice.reserveseat(this.reserving.value).subscribe(data => {
        if (data.apiResponse.id == 0) {
          this.seatservice.reserveseat(this.reserving.value)
          alert(data.apiResponse.desc)
          window.location.reload()
        }
        else //if (data.apiResponse.id==-1)
        {
          alert(data.apiResponse.desc)
        }
        console.log(data);
      });
    }
  }
}
