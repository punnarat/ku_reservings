import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { FormGroup, FormBuilder } from '@angular/forms';
import { SeatService } from 'src/app/seat.service';
import { ZoneService } from 'src/app/zone.service';
import { Zone } from 'src/app/zone.model';
import { HttpClient } from '@angular/common/http';
import { AuthenService } from 'src/app/authen.service';

@Component({
  selector: 'app-zones',
  templateUrl: './zones.component.html',
  styleUrls: ['./zones.component.css']
})
export class ZonesComponent implements OnInit {

  zone: Zone[] = [];
  seattypes:any;

  constructor(private router: Router, private modalService: NgbModal, private formBuilder: FormBuilder, private zoneservice: ZoneService, private http: HttpClient,
    private authenService:AuthenService) {
      console.log(this.authenService.userName);
      console.log(this.authenService.isAdmin);
  }

  ngOnInit() {
    this.zoneservice.getallzone().subscribe(result => {
      console.log(result);
      this.zone = result.data;
      this.zone = this.zone.filter(x => x.zoneCode == 'S')
      this.seattype()
    });
  }
  update(id) {
    console.log(id)
    this.router.navigate(['manage'])
    this.zoneservice.setData(id)
  }

  checkseat(seat, check) {
    console.log(check)
    status = "";
    console.log(seat);
    if (check.target.checked) {
      seat.seatStatusId = '10000000-0000-0000-0000-000000000000';
    }
    else {
      seat.seatStatusId = '00000000-0000-0000-0000-000000000000';
    }
    seat.seatstatus =
      console.log(seat);
    this.zoneservice.updatezone(seat).subscribe(result => {
      console.log(seat);
      this.ngOnInit()
      alert('Success');
    });
  }
  selecttype(seat,seatType) {
    console.log(seat);
    console.log(seatType);
    seat.seatTypeId=seatType;
    console.log(seat);
    this.zoneservice.updatezone(seat).subscribe(result => {
      console.log(seat);
      //this.ngOnInit()
      alert('Success');
    });
   }
  seattype() {
    var data = {
      'apiRequest': { 'action': 'list' }
    }
    this.http.post('http://roomcontrol.ctt-center.com/api/core/SeatType', data)
      .subscribe((res: any) => {
        this.seattypes = res.data
        console.log(res.data);
      })
  }
  
// popup
  open(content) {
    this.modalService.open(content, {size:"lg"});
  }
  openedit(contents) {
    this.modalService.open(contents, {size:"lg"});
  }
  
  // page
  zonea() {
    return this.router.navigate(['admin'])
  }
  zoneb() {
    return this.router.navigate(['zoneb'])
  }
  zonec() {
    return this.router.navigate(['zonec'])
  }
  zoned() {
    return this.router.navigate(['zoned'])
  }
  zonee() {
    return this.router.navigate(['zonee'])
  }
  zonef() {
    return this.router.navigate(['zonef'])
  }
  zoneg() {
    return this.router.navigate(['zoneg'])
  }
  zonep() {
    return this.router.navigate(['zonep'])
  }
  zones() {
    return this.router.navigate(['zones'])
  }

  admin() {
    return this.router.navigate(['admin'])
  }
  device() {
    return this.router.navigate(['device'])
  }
  adminmanage() {
    return this.router.navigate(['adminmanage'])
  }
  closeschedule() {
    return this.router.navigate(['closeschedule'])
  }
  reservingadminmanage() {
    return this.router.navigate(['reservingadminmanage'])
  }
  logout(){
    return this.router.navigate(['login'])
  }
  loginuser() {
    return this.router.navigate(['loginuser'])
  }
}
