import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { FormGroup, FormBuilder } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { DeviceService } from 'src/app/device.service';
import { Device } from 'src/app/device.model';
import { AuthenService } from 'src/app/authen.service';

@Component({
  selector: 'app-empty',
  templateUrl: './empty.component.html',
  styleUrls: ['./empty.component.css']
})
export class EmptyComponent implements OnInit {

  device: Device[] = [];
  constructor(private router: Router, private formBuilder: FormBuilder, private deviceservice: DeviceService, private http: HttpClient,
    private authenService: AuthenService) {
    console.log(this.authenService.userName);
    console.log(this.authenService.isAdmin);
  }

  ngOnInit() {
    this.deviceservice.getalldevice().subscribe(result => {
      console.log(result);
      this.device = result.data;
      this.device = this.device.filter(x => x.deviceTypeName == 'Empty')
    });
  }
  update(id) {
    console.log(id)
    this.router.navigate(['managedevice'])
    this.deviceservice.setData(id)
  }

  createdevice() {
    return this.router.navigate(['createdevice'])
  }

  remove(devicere) {
    var dialog = confirm(devicere.seatName)
    if (dialog == true) {
      this.deviceservice.removeDevice(devicere).subscribe(result => {
        console.log(result);
        alert('Delete Data!')
        this.ngOnInit()
      });
    }
  }
  windows() {
    return this.router.navigate(['device'])
  }
  mac() {
    return this.router.navigate(['maccom'])
  }
  scanner() {
    return this.router.navigate(['scanner'])
  }
  empty() {
    return this.router.navigate(['empty'])
  }
  admin() {
    return this.router.navigate(['admin'])
  }
  devicepage() {
    return this.router.navigate(['device'])
  }
  adminmanage() {
    return this.router.navigate(['adminmanage'])
  }
  closeschedule() {
    return this.router.navigate(['closeschedule'])
  }
  reservingadminmanage() {
    return this.router.navigate(['reservingadminmanage'])
  }
  logout() {
    return this.router.navigate(['login'])
  }
  loginuser() {
    return this.router.navigate(['loginuser'])
  }
}
