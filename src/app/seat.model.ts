import { CoreApiResponse } from './api.model';
export interface SeatResponse extends CoreApiResponse<Seat> {
}
export interface Seat {
    id: string,
    seatId: string,
    seatCode: string,
    seatName: string,
    scheduleDate: Date,
    periodId: string,
    periodCode: string,
    periodName: string,
    availablePeriod: 0,
    scheduleStatusId: string,
    scheduleStatusCode: string,
    scheduleStatusName: string,
    displayX: 0,
    displayY: 0,
    userName: string,
    seatStatusId: string,
    password: string
}
