import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { SeatService } from '../seat.service';
import { HttpClient } from '@angular/common/http';
import { AuthenService } from '../authen.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loginusers: FormGroup
  isAdmin = false;
  submitted = false;

  constructor(private router: Router, private modalService: NgbModal, private formBuilder: FormBuilder, private seatservice: SeatService, 
    private authenService:AuthenService,
    private http: HttpClient) {
    // const request = {apiRequest:{action:'login'},data:[{userName:'userx',password:'password@1'}]};
    // this.http.post('http://roomcontrol.ctt-center.com/api/core/user',request).subscribe(data=>{
    //   this.isAdmin = data.data[0].isAdmin;
    // alert(data.apiResponse.desc)
    //   console.log(data);
    // });

  }

  ngOnInit() {
    this.create()
  }
  get f() { return this.loginusers.controls; }
  loginadmin() {
    this.submitted = true;
    {
      if (this.loginusers.invalid) {
        return;
      }
      console.log(this.loginusers.value);
      this.seatservice.loginadmin(this.loginusers.value).subscribe(data => {
        //console.log(data);
        if (data.apiResponse.id == 0) {
          this.authenService.userName = data.data[0].userName;
          this.authenService.isAdmin = data.data[0].isAdmin;
          this.router.navigate(['reservingadminmanage'])
        }
        else {
          alert(data.apiResponse.desc)
        }
      });
    }
  }

  create() {
    this.loginusers = this.formBuilder.group({
      userName: ['', Validators.required],
      password: ['', Validators.required]
    })
  }
}
