import { CoreApiResponse } from './api.model';
export interface ZoneResponse extends CoreApiResponse<Zone> {
}
export interface Zone {
    id: string,
    code: string,
    name: string,
    zoneId: string,
    zoneCode: string,
    zoneName: string,
    zoneStatusId: string,
    zoneStatusCode: string,
    zoneStatusName: string,
    seatStatusId: string,
    seatStatusCode: string,
    seatStatusName: string,
    displayX: 0,
    displayY: 0,
    seatTypeId: string,
    seatTypeCode: string,
    seatTypeName: string
}
