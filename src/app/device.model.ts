import { CoreApiResponse } from './api.model';
export interface DeviceResponse extends CoreApiResponse<Device> {
}
export interface Device {
    id: string,
    name: string,
    description: string,
    deviceTypeId: string,
    deviceTypeCode: string,
    deviceTypeName: string,
    assetCode: string,
    serviceTo: string,
    ip: string,
    mac: string,
    seatId: string,
    seatCode: string,
    seatName: string
}
