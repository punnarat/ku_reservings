import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { SeatResponse, Seat } from './seat.model';


@Injectable()
export class SeatService {
    sharedData: any
    constructor(private httpClient: HttpClient) {
    }
    setData(data) {
        this.sharedData = data
    }
    getData() {
        console.log(this.sharedData)
        return this.sharedData
    }
    getallseat(): Observable<any> {
        // Javascript object
        const request = {
            apiRequest: { action: 'get_default' },
            data: [{
                
            }]
        };
        console.log(request)
        return this.httpClient.post<any>('http://roomcontrol.ctt-center.com/api/core/SeatSchedule', request);
    }
    getseatchange(): Observable<SeatResponse> {
        // Javascript object
        const request = {
            apiRequest: { action: 'get' },
            data: [{
                id: this.sharedData
            }]
        };
        console.log(request)
        return this.httpClient.post<SeatResponse>('http://roomcontrol.ctt-center.com/api/core/SeatSchedule', request);
    }
    getseat(financeDate: string): Observable<SeatResponse> {
        // Javascript object
        const request = {
            apiRequest: { action: 'get' },
            data: [{
                scheduleDate: financeDate,
                scheduleStatusId: "10000000-0000-0000-0000-000000000000"
            }]
        };
        console.log(request)
        return this.httpClient.post<SeatResponse>('http://roomcontrol.ctt-center.com/api/core/SeatSchedule', request);
    }
    get(): Observable<SeatResponse> {
        // Javascript object
        const request = {
            apiRequest: { action: 'get' },
            data: [{

            }]
        };
        console.log(request)
        return this.httpClient.post<SeatResponse>('http://roomcontrol.ctt-center.com/api/core/SeatSchedule', request);
    }
    getseatSearch(date, period): Observable<SeatResponse> {
        // Javascript object
        const request = {
            apiRequest: { action: 'get' },
            data: [{
                scheduleDate: date,
                periodCode: period,

            }]
        };
        console.log(request)
        return this.httpClient.post<SeatResponse>('http://roomcontrol.ctt-center.com/api/core/SeatSchedule', request);
    }
    reserveseat(data): Observable<SeatResponse> {
        // Javascript object
        const request = {
            apiRequest: { action: 'reserve' },
            data: [
                data
            ]
        };
        console.log(request)
        return this.httpClient.post<SeatResponse>('http://roomcontrol.ctt-center.com/api/core/SeatSchedule', request);
    }
    changereserveseat(seat: Seat): Observable<SeatResponse> {
        // Javascript object
        // var changeseat = [seat];
        const request = {
            apiRequest: { action: 'change' },
            data: [seat]
        };
        console.log(request)
        return this.httpClient.post<SeatResponse>('http://roomcontrol.ctt-center.com/api/core/SeatSchedule', request);
    }
    updateseat(seat: Seat): Observable<SeatResponse> {
        // Javascript object
        // var updateseat = [seat];
        const request = {
            apiRequest: { action: 'update' },
            data: [seat]
        };
        console.log(request)
        return this.httpClient.post<SeatResponse>('http://roomcontrol.ctt-center.com/api/core/SeatSchedule', request);
    }
    cancleseat(seat: Seat): Observable<SeatResponse> {
        const request = {
            apiRequest: { action: 'cancel' },
            data: [{
                id: seat.id,
            }]
        };
        console.log(request)
        return this.httpClient.post<SeatResponse>('http://roomcontrol.ctt-center.com/api/core/SeatSchedule', request);
    }
    seatzone(): Observable<any> {
        // Javascript object
        const request = {
            apiRequest: { action: 'list' },
            data: []
        };
        console.log(request)
        return this.httpClient.post<any>('http://roomcontrol.ctt-center.com/api/core/Seat', request);
    }
    login(data): Observable<any> {
        // Javascript object
        const request = {
            apiRequest: { action: 'login' },
            data: [data]
        };
        console.log(request)
        return this.httpClient.post<any>('http://roomcontrol.ctt-center.com/api/core/UserLogin', request);
    }
    loginemty(data): Observable<any> {
        // Javascript object
        const request = {
            apiRequest: { action: 'active' },
            data: [data]
        };
        console.log(request)
        return this.httpClient.post<any>('http://roomcontrol.ctt-center.com/api/core/UserLogin', request);
    }
    seatzonestatus(zonename: string): Observable<any> {
        // Javascript object
        const request = {
            apiRequest: { action: 'list' },
            data: [{ name: zonename }]
        };
        console.log(request)
        return this.httpClient.post<any>('http://roomcontrol.ctt-center.com/api/core/Seat', request);
    }
    loginadmin(data): Observable<any> {
        // Javascript object
        const request = {
            apiRequest: { action: 'login' },
            data: [data]
        };
        console.log(request)
        return this.httpClient.post<any>('http://roomcontrol.ctt-center.com/api/core/User', request);
    }
}