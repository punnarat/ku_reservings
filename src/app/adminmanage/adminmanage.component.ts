import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { FormGroup, FormBuilder } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { AdminService } from '../admin.service';
import { AuthenService } from '../authen.service';


@Component({
  selector: 'app-adminmanage',
  templateUrl: './adminmanage.component.html',
  styleUrls: ['./adminmanage.component.css']
})
export class AdminmanageComponent implements OnInit {
  adminlists: any;
  superadminlists: any;
  isAdmin:any;
  constructor(private router: Router, private modalService: NgbModal, private formBuilder: FormBuilder, private adminservice: AdminService, private http: HttpClient,
    private authenService:AuthenService) {
      this.isAdmin = this.authenService.isAdmin;
      console.log(this.authenService.userName);
      console.log(this.authenService.isAdmin);
  }

  ngOnInit() {
    this.superadminlist()
    this.adminlist()
  }
  superadminlist() {
    this.adminservice.adminlist().subscribe(result => {
      console.log(result);
      this.superadminlists = result.data;
      this.superadminlists = this.superadminlists.filter(x => x.isAdmin == true)
    });
  }
  adminlist() {
    this.adminservice.adminlist().subscribe(result => {
      console.log(result);
      this.adminlists = result.data;
      this.adminlists = this.adminlists.filter(x => x.isAdmin == false)
    });
  }
  update(id) {
    console.log(id)
    this.router.navigate(['editadmin'])
    this.adminservice.setData(id)
  }
  remove(admin) {
    // var dialog = confirm(admin.firstName)
    // if(dialog == true){ 
    this.adminservice.removeadmin(admin).subscribe(result => {
      console.log(result);
      alert('Delete Data!')
      this.ngOnInit()
    });
    // }
  }
  createadmin() {
    return this.router.navigate(['createadmin'])
  }
  admin() {
    return this.router.navigate(['admin'])
  }
  device() {
    return this.router.navigate(['device'])
  }
  adminmanage() {
    return this.router.navigate(['adminmanage'])
  }
  closeschedule() {
    return this.router.navigate(['closeschedule'])
  }
  reservingadminmanage() {
    return this.router.navigate(['reservingadminmanage'])
  }
  logout() {
    return this.router.navigate(['login'])
  }
  loginuser() {
    return this.router.navigate(['loginuser'])
  }
}
