import { CoreApiResponse } from './api.model';
export interface AdminResponse extends CoreApiResponse<Admin> {
}
export interface Admin {
    id: string,
    userName: string,
    password: string,
    firstName: string,
    lastName: string,
    isAdmin: boolean
}
