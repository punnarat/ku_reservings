import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { DeviceResponse, Device } from './device.model';

@Injectable()

export class DeviceService {

  sharedData: any

  constructor(private httpClient: HttpClient) {
  }
  setData(data) {
    this.sharedData = data
  }
  getData() {
    console.log(this.sharedData)
    return this.sharedData
  }
  getalldevice(): Observable<DeviceResponse> {
      // Javascript object
      const request = {
          apiRequest: { action: 'list' },
          data: [{ }]
      };
      console.log(request)
      return this.httpClient.post<DeviceResponse>('http://roomcontrol.ctt-center.com/api/core/Device', request);
  }
  getdevice(): Observable<DeviceResponse> {
    // Javascript object
    const request = {
        apiRequest: { action: 'get' },
        data: [{ id: this.sharedData }]
    };
    console.log(request)
    return this.httpClient.post<DeviceResponse>('http://roomcontrol.ctt-center.com/api/core/Device', request);
}
updatedevice(device: Device): Observable<DeviceResponse> {
    // Javascript object
    var updatedevice =  [device];
    const request = {
        apiRequest: { action: 'update' },
        data: updatedevice
    };
    console.log(request)
    return this.httpClient.post<DeviceResponse>('http://roomcontrol.ctt-center.com/api/core/Device', request);
}
removeDevice(device:Device): Observable<DeviceResponse> {
  // Javascript object
  const request = { apiRequest: { action: 'delete'}, data: [{id: device.id}]
  };
  return this.httpClient.post<DeviceResponse>('http://roomcontrol.ctt-center.com/api/core/Device', request);
}
}

